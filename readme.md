this has moved! find it at
[monsterware.dev](https://monsterware.dev/Fidgetcetera/fish_noisy_notifications)

# Noisy notifications

We got really tired of the way notifications are handled normally in apps. This fish script runs paplay and plays a sound file when you get a notification, monitoring Dbus to know that.
