#!/usr/bin/env fish

set default_sound_file "/usr/share/sounds/freedesktop/stereo/message.oga"
set default_sound_player "play"
#The app list takes app names in the format "NAME,do_notify/dont_notify,soundfile" where soundfile is optional.
set app_list "Clementine,dont_notify" "discord,do_notify,/home/fidgetcetera/Ring and Text Tones/iRingPro_MasterCollection_MP3/iRingPro_MasterCollection_MP3/Tones/Tek_mg_ACCESS-GRANTED_short_1.mp3" "Wire,do_notify,/home/fidgetcetera/Ring and Text Tones/iRingPro_MasterCollection_MP3/iRingPro_MasterCollection_MP3/Tones/Tek_mg_ACCESS-GRANTED_short_1.mp3" "Riot,do_notify,/home/fidgetcetera/Ring and Text Tones/iRingPro_MasterCollection_MP3/iRingPro_MasterCollection_MP3/Tones/Tek_mg_ACCESS-GRANTED_short_1.mp3" "org.gajim.Gajim,do_notify,/home/fidgetcetera/Ring and Text Tones/iRingPro_MasterCollection_MP3/iRingPro_MasterCollection_MP3/Tones/Tek_mg_ACCESS-GRANTED_short_1.mp3" "Ripcord,do_notify,/home/fidgetcetera/Ring and Text Tones/iRingPro_MasterCollection_MP3/iRingPro_MasterCollection_MP3/Tones/Tek_mg_ACCESS-GRANTED_short_1.mp3"


function play_sound -a sound_player -a sound_file
  command $sound_player "$sound_file" > /dev/null 2>&1; #Plays a sound with the given player (probably paplay) and the given sound file. I know it's a one-line function, but it just seems easier this way.
end



echo "starting up!"
play_sound $default_sound_player $default_sound_file; #Play a startup sound~

fish -c 'dbus-monitor --session interface="org.freedesktop.Notifications",member="Notify" &
dbus-monitor --session interface="org.gtk.Notifications",member="AddNotification"'| #Monitor dbus for freedesktop and gtk notifications. You can't have more than one interface here. It's the worst.
awk '/method call/{getline; print; fflush(stdout)}'| #Use awk to look for the text "method call" at the start of a new notification, then look for the new line *after* that that says the name of the notification-giving app.
awk -F'\"' '{print $2; fflush(stdout);}'| #Use awk again to pick out just the app name.
while read output #read the app names as they come in.

  set -g on_list false; #Is the current output in the app list?
  for app in $app_list #check against the individual app entries!
    set app_array (string split "," $app); #Make an array out of the string format. First element is the name!

    if test $app_array[1] = $output
      set -g on_list true; #The current output is on the app list.
    end
    if test $output = $app_array[1]; and test $app_array[2] = "do_notify"; and test (count $app_array) -eq 3 #If the current output is this iteration of the app array, and the array says to notify, and there is a sound file (third section of the string)
      echo "Notification from: "$output". Playing a custom sound!" && play_sound $default_sound_player "$app_array[3]"; #Play the custom sound.
    else if test $output = $app_array[1]; and test $app_array[2] = "do_notify" #Play a standard notification. This is functionally no different than line 40, but allows for allow-list or black-lists depending on if line 40 is commented out.
      echo "Notification from: "$output && play_sound $default_sound_player $default_sound_file;
    else if test $output = $app_array[1]; and test $app_array[2] = "dont_notify" #Don't play a notification sound.
      echo "Notification from: "$output". Not playing a sound!";
    end
  end
  if not $on_list #If the current output is not on the app list, just play a default sound. You can comment this line out to only notify for specific apps.
    echo "Notification from: "$output && play_sound $default_sound_player $default_sound_file; #Play a sound and echo out the name of an app you got a notif from. To only play sounds for apps on your list, comment out this line.
  end
end
